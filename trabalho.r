library(relaimpo)
library(caret)
library(randomForest)
library(rattle)
library(kernlab)

set.seed(123)
raw_data <- read.csv(file.choose(), sep=";")
#REMOVE MISSING DATA
raw_data <- na.omit(raw_data) 

#APPLYING FEATURE SELECTION
lmMod <- lm(raw_data$quality ~ . , data = raw_data) 
relImportance <- calc.relimp(lmMod, type = "lmg", rela = TRUE) 
sort(relImportance$lmg, decreasing=TRUE)

#REMOVING BAD FEATURES
data <- raw_data[,c("quality","volatile.acidity","density","sulphates","alcohol", "total.sulfur.dioxide",
                    "citric.acid","chlorides", "fixed.acidity","pH")]
#BAD FEATURES: residual.sugar free.sulfur.dioxide"

#ADJUSTING DATA PARTITION
data$quality <- as.factor(data$quality)
index <- createDataPartition(data$quality, p= 0.80, list=FALSE)
Train <- data[index,]
Test <- data[-index,]

#APPLYING RANDOM FOREST
rf <- randomForest(Train$quality~., data=Train, ntree=50, do.trace=T, importance=T)
pred.rf <- predict(rf, newdata=Test, type="class")
confusionMatrix(pred.rf, Test$quality)

